﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        //TODO: Add Unit Tests
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
            };
            return partner;
        }

        //1
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnBadRequest()
        {
            //Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            SetPartnerPromoCodeLimitRequest request = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        //2
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnBadRequest()
        {
            //Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            Partner partner = CreateBasePartner();
            partner.IsActive = false;

            SetPartnerPromoCodeLimitRequest request = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //3_1
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitZeroizeNumberIssuedPromoCodes_PartnerNumberIssuedPromoCodesHasZero()
        {

            //Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var partnerId = fixture.Create<Guid>();

            var partnerLimit = fixture.Build<PartnerPromoCodeLimit>()
                .Without(l => l.Partner)
                .Without(l => l.CancelDate)
                .With(l => l.PartnerId, partnerId)
                .Create();


            var partner = fixture.Build<Partner>()
                .Without(p => p.PartnerLimits)
                .With(p => p.Id, partnerId)
                .Do(p => p.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Do(p => p.PartnerLimits.Add(partnerLimit))
                .Create();


            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                 .With(r => r.Limit, 4).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        //3_2
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitZeroizeNumberIssuedPromoCodes_PartnerNumberIssuedPromoCodesNotZero()
        {

            //Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var partnerId = fixture.Create<Guid>();

            var partnerLimit = fixture.Build<PartnerPromoCodeLimit>()
                .Without(l => l.Partner)
                .Without(l => l.CancelDate)
                .With(l => l.PartnerId, partnerId)
                .With(l=>l.Limit,1)
                .Create();


            var partner = fixture.Build<Partner>()
                .Without(p => p.PartnerLimits)
                .With(p => p.Id, partnerId)
                .With(p =>p.NumberIssuedPromoCodes,1)
                .Do(p => p.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Do(p => p.PartnerLimits.Add(partnerLimit))
                .Create();


            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                 .With(r => r.Limit, 4).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(1);
        }

        //4
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitDisablePreviousLimit_PreviousLimitCancelDateIsNotNull()
        {
            
            //Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var partnerId = fixture.Create<Guid>();
            
            var partnerLimit = fixture.Build<PartnerPromoCodeLimit>()
                .Without(l=>l.Partner)
                .Without(l => l.CancelDate)
                .With(l=>l.PartnerId, partnerId)
                .Create();
           
            
            var partner = fixture.Build<Partner>()
                .Without(p=> p.PartnerLimits)
                .With(p=>p.Id, partnerId )
                .Do(p=>p.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Do(p=>p.PartnerLimits.Add(partnerLimit))
                .Create();
            

            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                 .With(r => r.Limit, 4).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            partnerLimit.CancelDate.Should().NotBeNull();
        }

        //5
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimitWithValueZero_ReturnBadRequest()
        {

            //Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            var partner = fixture.Build<Partner>()
                .Without(p => p.PartnerLimits)
                .With(p=>p.IsActive, true)
                .Do(p => p.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Create();

            var partnerId = partner.Id;

            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                 .With(r => r.Limit, 0).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //6
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_LimitWasAdded()
        {

            //Arrange
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            var partner = fixture.Build<Partner>()
                .Without(p => p.PartnerLimits)
                .With(p => p.IsActive, true)
                .Do(p => p.PartnerLimits = new List<PartnerPromoCodeLimit>())
                .Create();

            var partnerId = partner.Id;

            var request = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                 .With(r => r.Limit, 4).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert

            _partnersRepositoryMock.Verify(p => p.UpdateAsync(partner), Times.Once);
        }
    }
}